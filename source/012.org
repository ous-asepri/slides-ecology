#+OPTIONS: num:nil
#+TITLE: 自分の生きている生態系を考える
#+SUBTITLE: 生態学：第1回の2
#+AUTHOR: 担当：竹ノ下祐二
#+EMAIL: y-takenoshita@ous.ac.jp
#+DATE: 2024/04/12
#+REVEAL_MISCINFO: 岡山理科大学理学部動物学科2024年度春学期開講科目
#+REVEAL_MULTIPLEX_ID: abbf73fb79a4ced4
#+REVEAL_MULTIPLEX_SECRET: 17128142078388853224
* このセクションの目標

#+ATTR_HTML: :style font-size:140%;text-align:center;
自分の生きる生態系を *実感* をもって意識する

#+ATTR_REVEAL: :frag (appear)
#+ATTR_HTML: :style text-align:center;
そして生態学をちゃんと学ぼうというモチベーションを高める


* そもそも生態系って何？
:PROPERTIES:
:REVEAL_EXTRA_ATTR: class="onlyheading"
:END:

** 考えるヒント：10年前の自分
- 10年前、あなたはどこで何をしていましたか？
  - 2014年
    - 田中将大投手がNYヤンキースに入団、1年目から13勝
    - サッカーWCブラジル大会、日本代表は「自分たちのサッカー」で予選敗退
- それから今までの10年間、あなたの人生の歩みを振り返ってみてください。
  - どのような経緯をへて、「いま・ここ」にいる？

** 「今の自分」の身体は「10年前の自分」の身体ではない！
- 人体を構成する元素は代謝によって不断に入れ替わっている


#+CAPTION: 臓器ごとの元素の置換速度
#+ATTR_HTML: :class header-column :style font-size:80%;
| 脳         | 1ヶ月で約40%。遅い成分も約1年                          |
| 胃の粘膜   | 約3日                                                  |
| 腸の微繊毛 | 約1日                                                  |
| 肝臓       | 1ヶ月で約96％。遅い成分も約1年                         |
| 腎臓       | 1ヶ月で約90％。遅い成分も約1年                         |
| 筋肉       | 1ヶ月で約60％。遅い成分も約200日                       |
| 皮膚       | 約1ヶ月                                                |
| 血液       | 100〜200日                                             |
| 骨         | 幼児期は1.5年、成長期は2年弱、成人は2.5年、高齢者は3年 |


** 考え直し：今の自分と10年前の自分
- 10年前、あなたはどこで何をしていましたか？
  - 「今の自分」を構成する物質は、10年前は *どこの何として存在していた* のだろうか
    ？
- 10年間の自分の歩みを振り返る
  - 「今の自分」を構成する物質は、10年前の状態から *どこをどのように経由して* 自分の身体になったのだろうか？
  - 「10年前の自分」を構成していた物質は、その後10年間 *どこをどのように経由し、* 今は *どこの何として存在している* のだろうか？


#+ATTR_REVEAL: :frag (appear)
#+ATTR_HTML: :style font-size:200%;text-align:center;
*物質循環* 


** 物質循環と生物間相互作用
- 動物の身体の元素組成は種によって大差ない
  - 酸素、炭素、水素、窒素で全重量の約98%
  - しかし、炭素や窒素は元素として摂取しても利用できない
    - /生物的・非生物的プロセス/ で合成された有機物＝栄養素が必要
- 動物の栄養要求には大きな種間・種内変異がある
  - 種や個体によって異なる物質循環の系がある
  - その系を実現するために生物—生物、生物—非生物のあいだでさまざまな相互作用


#+ATTR_REVEAL: :frag (appear)
#+ATTR_HTML: :style font-size:200%;text-align:center;
*生態系（エコシステム）*


* 生態学的な視点で生活・社会・未来を考える
:PROPERTIES:
:REVEAL_EXTRA_ATTR: class="onlyheading"
:END:

** 自分は何を食べているのか：その1
- 某ファストフード店での食事
  #+ATTR_REVEAL: :frag (appear)
  - チーズバーガー
    - バンズ： /コーンスターチ/ 、小麦粉
    - パティ：牛→穀物飼料＝ /トウモロコシ/
    - チーズ：牛→穀物飼料＝ /トウモロコシ/
  - チキンナゲット
    - 鶏←穀物飼料＝ /トウモロコシ/
    - 衣： /コーンスターチ/
    - 揚げ油： /コーン油/
  - 清涼飲料水
    - 水
    - 果糖← /コーンシロップ/ 

** 自分は何を食べているのか：その2
- 国際関係の変化は生態系の変化

  
#+REVEAL_HTML: <iframe src="https://www.maff.go.jp/j/pr/aff/2312/spe1_04.html" width="1200" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

#+ATTR_HTML: :style font-size:60%;text-align:right;
https://www.maff.go.jp/j/pr/aff/2312/spe1_04.html


** 考えてみよう

#+ATTR_REVEAL: :frag (appear)
1. 自分の生きている物質循環の系はどのようなものだろうか？それは、望ましいものだろうか？
2. さまざまな社会課題を生態系の観点から考えなおしてみよう
   - 地産地消
   - breastfeeding
3. 元素レベルで完全に置換しているはずの「10年前の自分」と「今の自分」の同一性はいったい何によって保たれているのだろうか？
   - 同様の例（「テセウスの船」のパラドクス）
     - 1985年と2023年の阪神タイガースは /同じチーム/ ？
     - 平安時代と現代の日本語は /同じ言語/ ？

* 参考書
- 「雑食動物のジレンマ---ある4つの食事の自然誌---」
  - マイケル・ポーラン（著）、ラッセル秀子（訳）
  - 東洋経済新報社　2009年


[[file:img/omunivores-dilemma.png]]

#+ATTR_HTML: :style font-size:70%;text-align:right;
[[https://str.toyokeizai.net/books/9784492043523/][出版社リンク]]
