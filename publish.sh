#!/bin/sh
emacs --batch --no-init-file \
      --eval "(load-file \"./setup.el\")" \
      --eval "(org-publish \"site-public\" t)" \
      --eval "(message \"Build complete!\")"
